﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RocketCore.Gateways.TCP.Messages
{
    class MarginCallMsg : IJsonSerializable
    {
        private int user_id;
        private DateTime dt_made;

        internal MarginCallMsg(int user_id, DateTime dt_made) //конструктор сообщения
        {
            this.user_id = user_id;
            this.dt_made = dt_made;
        }

        public string Serialize()
        {
            return JsonManager.FormTechJson((int)MessageTypes.NewMarginCall, user_id, dt_made);
        }
    }
}
